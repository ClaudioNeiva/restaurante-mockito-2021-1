package br.ucsal._20211.testequalidade.restaurante.business;

import br.ucsal._20211.testequalidade.restaurante.domain.Comanda;
import br.ucsal._20211.testequalidade.restaurante.domain.Item;
import br.ucsal._20211.testequalidade.restaurante.domain.Mesa;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoComandaEnum;
import br.ucsal._20211.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal._20211.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal._20211.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal._20211.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal._20211.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal._20211.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBO {

	private MesaDao mesaDao;

	private ComandaDao comandaDao;

	private ItemDao itemDao;

	public RestauranteBO(MesaDao mesaDao, ComandaDao comandaDao, ItemDao itemDao) {
		this.mesaDao = mesaDao;
		this.comandaDao = comandaDao;
		this.itemDao = itemDao;
	}

	public Integer abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException {
		Mesa mesa = mesaDao.obterPorNumero(numeroMesa);
		if (SituacaoMesaEnum.LIVRE.equals(mesa.getSituacao())) {
			Comanda comanda = comandaDao.criarComanda(mesa);
			mesa.setSituacao(SituacaoMesaEnum.OCUPADA);
			return comanda.getCodigo();
		}
		throw new MesaOcupadaException(numeroMesa);
	}

	public void incluirItemComanda(Integer codigoComanda, Integer codigoItem, Integer qtdItem)
			throws RegistroNaoEncontrado {
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		Item item = itemDao.obterPorCodigo(codigoItem);
		comanda.incluirItem(item, qtdItem);
	}

	public Double fecharComanda(Integer codigoComanda) throws RegistroNaoEncontrado, ComandaFechadaException {
		Comanda comanda = comandaDao.obterPorCodigo(codigoComanda);
		if (SituacaoComandaEnum.ABERTA.equals(comanda.getSituacao())) {
			comanda.setSituacao(SituacaoComandaEnum.FECHADA);
			return comanda.calcularTotal();
		}
		throw new ComandaFechadaException(codigoComanda);
	}

}
