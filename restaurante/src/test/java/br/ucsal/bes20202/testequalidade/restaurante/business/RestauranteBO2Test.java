package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO2Test {

	/**
	 * Método a ser testado:
	 * 
	 * public Integer abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException {
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso,
	 * retornando o número da comanda e mudando a situação da mesa para Ocupada.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao, setSituacao e getCodigo;
	 * 
	 */
	public void testarAbrirComandaMesaLivre() {
	}
}
