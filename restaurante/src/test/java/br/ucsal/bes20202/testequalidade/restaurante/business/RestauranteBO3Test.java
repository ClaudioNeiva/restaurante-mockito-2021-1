package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO3Test {

	/**
	 * Método a ser testado:
	 * 
	 * public void incluirItemComanda(Integer codigoComanda, Integer codigoItem,
	 * Integer qtdItem) throws RegistroNaoEncontrado, ComandaFechadaException.
	 * 
	 * Verificar se a inclusão de um item válido em uma comanda válida apresenta
	 * sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Item;
	 * 
	 * 2. A comanda deve ser mocada, pois o comportamento do método incluirItem da
	 * classe Comanda não deve impactar no teste do método incluirItemComanda;
	 * 
	 * 3. Como o método incluirItemComanda é void, você deverá usar o verify para
	 * verificar se o método incluirItem da comanda foi chamado com os parâmetros
	 * corretos.
	 * 
	 */
	public void testarIncluirItemComanda() {
	}
}
