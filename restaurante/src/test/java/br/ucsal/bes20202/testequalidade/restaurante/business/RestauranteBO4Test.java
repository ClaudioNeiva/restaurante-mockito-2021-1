package br.ucsal.bes20202.testequalidade.restaurante.business;

public class RestauranteBO4Test {

	/**
	 * Método a ser testado:
	 * 
	 * public Double fecharComanda(Integer codigoComanda) throws
	 * RegistroNaoEncontrado, ComandaFechadaException
	 * 
	 * Verificar se o fechamento de uma comanda ABERTA retorna o valor total e a
	 * situaçao da mesma muda para FECHADA.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar as classes Item;
	 * 
	 * 2. Você deve mocar a classe Comanda, além da classe ComandaDao, pois o método
	 * calcularTotal apresenta lógica complexa, não devendo ser testado neste teste
	 * unitário do RestauranteBO.
	 * 
	 */
	public void testarFecharComandaAberta() {
	}
}
